FROM node:21

RUN mkdir -p /blitz
WORKDIR /blitz

COPY package.json .
COPY package-lock.json .
RUN npm install

COPY . .
RUN npm run build

EXPOSE 3000

CMD ["bash", "entrypoint.sh"]
