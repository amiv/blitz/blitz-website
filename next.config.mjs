import mdx from "@next/mdx";
const withMdx = mdx();

/** @type {import('next').NextConfig} */
const nextConfig = {
  // Configure `pageExtensions` to include MDX files
  pageExtensions: ["js", "jsx", "mdx", "ts", "tsx"],
  // Optionally, add any other Next.js config below
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "blitz-archive-backend.amiv.ethz.ch",
        port: "",
        pathname: "/preview/**",
      },
    ],
  },
};

export default withMdx(nextConfig);
