import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function ArchiveDE() {
  const content = `
## Ausgaben findest Du im praktischen [Online-Archiv](https://blitz-archive.amiv.ethz.ch/)

Hier im blitz Archiv findest du alle blitz-Ausgaben die es je gegeben hat, beginnend mit dem ‘AMIV Extrablatt’ im Jahre 1960. Viel Spass beim Stöbern!

Leider ist unser Archiv nicht zu 100% vollständig. Falls du noch eine dieser Ausgabe aus Studienzeiten bei dir zu Hause hast, melde dich gerne bei [praesident@blitz.ethz.ch](mailto:praesident@blitz.ethz.ch)

- 1969FS Ausgabe 17, 19
- 1991HS Ausgabe 09, 14
- 1992FS Ausgabe 17, 22, 24, 29
- 1994HS Ausgabe 4
- 1996FS Ausgabe 8
- 2001FS Ausgabe 11 - kaputte Seite 31 (Ersatz gesucht)
- 2002FS Ausgabe 9
- 2002HS Ausgabe 3
- 2017HS Ausgabe 6
  `;
  return <MDXRemote source={content} components={components} />;
}
