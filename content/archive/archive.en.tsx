import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function ArchiveEN() {
  const content = `
## Issues can be found in the practical [online archive](https://blitz-archive.amiv.ethz.ch/)

Here in the blitz archive you will find all the blitz issues that have ever existed, starting with the 'AMIV Extrablatt' in 1960. Have fun browsing!

Unfortunately, our archive is not 100% complete. If you still have one of these issues from your student days at home, please contact [praesident@blitz.ethz.ch](mailto:praesident@blitz.ethz.ch)

- 1969FS Issue 17, 19
- 1991HS Issue 09, 14
- 1992FS Issue 17, 22, 24, 29
- 1994HS Issue 4
- 1996FS Issue 8
- 2001FS Issue 11 - kaputte Seite 31 (Ersatz gesucht)
- 2002FS Issue 9
- 2002HS Issue 3
- 2017HS Issue 6
  `;
  return <MDXRemote source={content} components={components} />;
}
