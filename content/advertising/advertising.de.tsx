import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function AdvertisingDE() {
  const content = `
## Über den blitz

Seit über 50 Jahren informiert und unterhält die Fachvereinszeitschrift blitz die Studierenden des [AMIV](https://amiv.ethz.ch), des Akademischen Maschinen- und Elektroingenieur Vereins an der ETH Zürich. Jede zweite Woche erscheint eine neue Ausgabe mit einer Auflage von 1100 Stück, die in den Vorlesungen verteilt und ausgelegt wird, und laut einer Umfrage ca. 1800 Studierende regelmässig erreicht. Wir nehmen sehr gerne Inserate entgegen, alle Informationen zu Preisen und Erscheinungsdaten für das Herbstsemester 2024 können Sie dem [Infoblatt FS2025](/files/Blitz_Infoblatt_FS25.pdf) entnehmen.

## Was im blitz inserieren?

Ob Sie auf ein Event aufmerksam machen, ihr Unternehmen vorstellen oder in die Wahrnehmung der Studierenden rücken wollen, der blitz bietet Ihnen dazu die perfekte Plattform. Für kreative Ideen und individuelle Vorschläge sind wir natürlich immer offen, kontaktieren Sie uns per Mail [werbung@blitz.ethz.ch](mailto:werbung@blitz.ethz.ch) und wir setzen uns zusammen.

## Warum im blitz inserieren?

Unsere Leserschaft sind die zukünftigen Ingenieure und Ingenieurinnen, die zahlreiche Studiengänge am Department für Elektrotechnik und Informationstechnologie ([D-ITET](https://ee.ethz.ch/)) und dem Department für Maschinenbau und Verfahrenstechnik ([D-MAVT](https://mavt.ethz.ch/)) studieren.

Die Interessen unserer Studierenden können folgenden Vertiefungen (und vielen weiteren) zusammengefasst werden:

Bachelor Maschinenbau:

- Biomedizinische Technik
- Energy, Flows and Processes
- Mechatronics
- Mikrosysteme und Nanotechnologie
- Produktionstechnik
- Design, Mechanics and Materials
- Management, Technology and Economics

Bachelor Elektrotechnik:

- Kommunikation
- Computer und Netzwerke
- Elektrotechnik und Photonik
- Energie und Leistungselektronik
- Biomedizinische Technik

Im Master umfasst das Angebot des D-MAVT und D-ITET folgende Studiengänge:

- Biomedical Engineering
- Elektrotechnik und Informationstechnologie
- Maschineningenieurwissenschaften
- Micro and Nanosystems
- Nuclear Engineering
- Robotics, System and Control
- Quantum Engineering
- Verfahrenstechnik

Die Studierenden dieser Studiengänge bilden die Zielgruppe des blitz und sind somit potenzielle Leser:innen Ihrer Anzeigen.

## Welche Inhalte werden im blitz publiziert?

Im blitz schreiben wir über alles, was die Studierenden bewegt. Zum einen Information über das Studium (Hochschulpolitik Kolumne), Angebote und Neuigkeiten aus dem Fachverein liest man in der Präsi-Kolumne und Interviews mit Professoren und Professorinnen die einen Einblick in die Forschung und hinter die Kulissen des Vorlesungsbetriebs geben. Jede Ausgabe hat ein Ausgabenthema, zu dem mehrere Artikel und Beiträge geschrieben werden, besonderer Beliebtheit erfreuen sich die Rätsel und die Notenstatistiken der Blockprüfungen. Der blitz ist als unabhängige Plattform für kritische und konstruktive Beiträge bekannt, die oft in Satire verpackt sind, und erfreut sich nicht zuletzt deswegen einer grossen Leserschaft.

## Weitere Information

Zusätzliche Informationen können Sie dem Infoblatt entnehmen, in welchem sich auch die aktuellen Inseratspreise befinden. Bei weiteren Fragen können Sie uns gerne unter [werbung@blitz.ethz.ch](mailto:werbung@blitz.ethz.ch) kontaktieren.
  `;
  return <MDXRemote source={content} components={components} />;
}
