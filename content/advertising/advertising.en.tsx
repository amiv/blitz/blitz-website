import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function AdvertisingEN() {
  const content = `
## About blitz

For over 50 years, the professional association magazine blitz has been informing and entertaining the students of the [AMIV](https://amiv.ethz.ch), the Academic Mechanical and Electrical Engineering Association at ETH Zurich. A new issue is published every other week with a circulation of 1100 copies, which is distributed and displayed in lectures and, according to a survey, regularly reaches around 1800 students. We are happy to accept advertisements, all information on prices and publication dates for the autumn semester 2024 can be found in the [Infoblatt FS2025](/files/Blitz_Infoblatt_FS25.pdf).

## What to advertise in blitz

Whether you want to draw attention to an event, present your company or raise awareness among students, blitz offers you the perfect platform. Of course, we are always open to creative ideas and individual suggestions, just contact us by e-mail [werbung@blitz.ethz.ch](mailto:werbung@blitz.ethz.ch) and we will get together.

## Why advertise in blitz

Our readers are the future engineers who study numerous degree programs at the Department of Electrical Engineering and Information Technology ([D-ITET](https://ee.ethz.ch/)) and the Department of Mechanical and Process Engineering ([D-MAVT](https://mavt.ethz.ch/)).

The interests of our students can be summarized in the following specializations (and many more):

Bachelor Mechanical Engineering:

- Biomedical Technology
- Energy, Flows and Processes
- Mechatronics
- Microsystems and Nanotechnology
- Production Technology
- Design, Mechanics and Materials
- Management, Technology and Economics

Bachelor Electrical Engineering:

- Communication
- Computers and Networks
- Electrical Engineering and Photonics
- Energy and Power Electronics
- Biomedical Technology

The D-MAVT and D-ITET offer the following Master's degree programs:

- Biomedical Engineering
- Electrical Engineering and Information Technology
- Mechanical Engineering
- Micro and Nanosystems
- Nuclear Engineering
- Robotics, System and Control
- Quantum Engineering
- Process Engineering

The students of these degree programs form the target group of the blitz and are therefore potential readers of your advertisements.

## Which content is published in blitz?

In blitz we write about everything that moves students. On the one hand, information about studying (university politics column), offers and news from the student association can be read in the president's column and interviews with professors that give an insight into research and behind the scenes of lectures. Each issue has a theme on which several articles and contributions are written; the puzzles and the grade statistics of the block examinations are particularly popular. The blitz is known as an independent platform for critical and constructive articles, which are often wrapped in satire, and enjoys a large readership not least because of this.

## Further Information

Additional information can be found in the information sheet, which also contains the current advertisement prices. If you have any further questions, please do not hesitate to contact us at [werbung@blitz.ethz.ch](mailto:werbung@blitz.ethz.ch).
  `;
  return <MDXRemote source={content} components={components} />;
}
