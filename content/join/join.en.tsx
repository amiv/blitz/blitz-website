import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function JoinEN() {
  const content = `
## Write an article or join in?

The blitz editorial team is always on the lookout for new authors, editors, layout designers or distributors. With us you can give free rein to your creativity, whether through articles, puzzles, comics or similar. You can see what content is possible in the archive. You can choose whether you want to publish something on a regular basis or simply on topics that suit you. Of course, not every creative outpouring of yours has to fit the respective issue topic.

## What are the tasks of an author/editor/layouter/distributor?

**Author:** As an author, you write articles on topics that you like. You are completely free to choose your own topics, but you can always use the next issue topic as a guide. You can also take part in interviews with professors and get to know your professors in a different atmosphere. Don't be afraid that your style is not yet advanced enough, thanks to our team of editors, initial difficulties in writing can be overcome so that you can develop your own style in peace.

**Editor:** As an editor, you correct the authors' texts for style, spelling and grammar. This also includes the freedom to reorganize a text significantly, as long as the content remains the same; if in doubt, you can sit down with the author. If possible, you should also take a look at other articles, proofreading can be demanding and even the best sometimes overlook something.

**Layout:** As a layouter, you are responsible for making all texts and images appealing and print-ready - nobody wants to read a magazine that looks like it was created by a primary school student in Word. With your creativity and eye for design, you can create a masterpiece of print.

**Distributor:** As a distributor, you make sure that mainly first-year students receive the blitz in their hands during lectures. In addition, you lay out the issues at the designated locations and keep track of which locations have leftover issues to make fewer available there during the bi-weekly distribution. It must also be taken into account that a certain amount of issues must be held back for archiving, specimen copies, etc.

If you are interested in attending one of the bi-weekly meetings, please contact us at [redaktion@blitz.ethz.ch](mailto:redaktion@blitz.ethz.ch). If you value anonymity, you can of course also submit your content anonymously to us at [artikel@blitz.ethz.ch](mailto:artikel@blitz.ethz.ch).
  `;
  return <MDXRemote source={content} components={components} />;
}
