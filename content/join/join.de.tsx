import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function JoinDE() {
  const content = `
## Artikel schreiben oder mitmachen?

Die blitz Redaktion ist immer auf der Suche nach neuen Autor:innen, Lektor:innen, Layouter:innen oder Verteiler:innen. Bei uns kannst Du deiner Kreativität freien Lauf lassen, ob durch Artikel, Rätsel, Comics oder ähnliches. Was alles möglich ist an Inhalt, kannst Du dir im Archiv anschauen. Du hast die Wahl, ob du regelmässig etwas veröffentlich möchtest oder auch einfach bei Ausgabenthemen die Dir liegen. Natürlich muss auch nicht jeder kreative Erguss von Dir zum jeweiligen Ausgabenthema passen.

## Welche Aufgaben hat ein:e Autor:in/Lektor:in/Layouter:in/Verteiler:in?

**Autor:in:** als Autor:in schreibst du Artikel zu Themengebieten, die Dir gefallen. Dabei steht dir die Themenwahl vollkommen frei, jedoch kannst Du dich immer am nächsten Ausgabenthema orientieren. Zusätzlich kannst Du dich auch bei Professoreninterviews beteiligen und so auch Deine Professor:innen in einer anderen Atmossphäre kennenlernen. Habe auch keine Angst davor, dass dein Stil noch nicht weit genug sei, dank unserem Lektorenteam können anfängliche Schwierigkeiten beim Schreiben aufgefangen werden, sodass Du in Ruhe deinen eigenen Stil entwickeln kannst.

**Lektor:in:** als Lektor:in korrigierst Du den Müll von oben die Texte der Autor:innen auf Stil, Rechtschreibung und Grammatik. Dazu zählt auch die Freiheit, einen Text stark umzugestalten, solange der Inhalt sinngemäss erhalten bleibt, im Zweifelsfall kannst Du Dich dabei mit der Autorin zusammensetzen. Zusätzlich solltest Du nach Möglichkeit auch einen Blick auf andere Artikel werfen, lektorieren kann fordernd sein und auch die Besten übersehen Mal etwas.

**Layouter:in:** als Layouter:in bist du dafür zuständig, alle Texte und Bilder ansprechend und druckfertig zu machen – niemand will eine Zeitschrift lesen, die aussieht, als hätte sie ein Primarschüler in Word gemacht. Mit deiner Kreativität und deinem Auge für Design kannst du ein Meisterwerk des Druckes schaffen.

**Verteiler:in:** als Verteiler:in kümmerst Du dich darum, dass hauptsächlich die Studierenden im ersten Jahr den blitz in den Vorlesungen in die Hand gedrückt bekommen. Zusätzlich legst Du die Ausgaben bei den vorgesehenen Orten aus und behältst beim zweiwöchentlichen Verteilen den Überblick darüber, an welchen Orten Ausgaben übrig geblieben sind um dort weniger bereitzustellen. Auch muss berücksichtigt werden, dass eine gewisse Menge an Ausgaben für Archivierung, Belegexemplare etc. zurückgehalten werden muss.

Falls Du Interesse hast, doch einmal bei einer der zweiwöchentlichen Sitzungen vorbeizuschauen dann melde Dich doch bei uns unter [redaktion@blitz.ethz.ch](mailto:redaktion@blitz.ethz.ch). Wenn Du Wert auf Anonymität legst, kannst Du natürlich auch deine Inhalte anonym bei uns einreichen, unter [artikel@blitz.ethz.ch](mailto:artikel@blitz.ethz.ch).
  `;
  return <MDXRemote source={content} components={components} />;
}
