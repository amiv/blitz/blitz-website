import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function MainEN() {
  const content = `
## Publication dates and editorial deadline for the spring semester 2025
- **blitz 07: Ja/Nein/Vielleicht**

  Edit. Deadline 26.02.2025, Publication date 10.03.2025
- **blitz 08: mavt(at)ethz.ch**

  Edit. Deadline 12.03.2025, Publication date 24.03.2025
- **blitz 09: Nutri Score**

  Edit. Deadline 26.03.2025, Publication date 07.04.2025
- **blitz 10: Hasskommentarschmiede**

  Edit. Deadline 09.04.2025, Publication date 28.04.2025
- **blitz 11: Brainrot**

  Edit. Deadline 30.04.2025, Publication date 12.05.2025
- **blitz 12: Glücksspiel**

  Edit. Deadline 14.05.2025, Publication date 26.05.2025
    
## More issues in the [archive](https://blitz-archive.amiv.ethz.ch/)

The history of the blitz goes back to the 60s. All old issues, as well as the new ones, can be found in our [archive](https://blitz-archive.amiv.ethz.ch/).

The blitz04 - audio book of HS2021 is also available as an audio book: [Audio book](https://blitz.ethz.ch/grav/hoerbuch/)

## We are looking for members!

Dear AMIV members

The blitz is looking for writing support! Do you want to blow off steam about your studies, do you have a project that too few people know about, are you plagued by a world-weariness that needs to be communicated, do your studies offer you too few opportunities to give free rein to your creativity in the form of printable products or do you like to throw letters on paper in the hope that something readable will come out? Then you've come to the right place!

At blitz, you can expect a friendly editorial team, a sumptuous end-of-semester meal if you actively participate and perhaps more. If you're interested, get in touch at [info@blitz.ethz.ch](mailto:info@blitz.ethz.ch)

Dozens of active and former members of the editorial team confirm the benefits of getting involved:

**Alexander, ex-editor-in-chief:** *Actually I wanted to distribute issues to eat Chinese food for free, now I'm allowed to rule over the largest professional association magazine at ETH and write such appeals, 10/10*

**Manuel, long-standing member of the editorial team:** *Which journal? Are you telling me that you published my texts that I sent you? Is that public? Now I also realize why no company wants to hire me after the background check! 0/10*

**Julian, former layout artist and quaestor:** *During my time at blitz, I was not only able to meet great people, have a great time and enjoy free lunch, but also realize myself and leave a mark. No matter what you do at blitz, it's worth it! And it's doubly so: firstly for you, as you can implement your own creative ideas, and secondly for your fellow students, who get to experience a little distraction or interesting insights every two weeks. Come to an editorial meeting right away to leave your impact during your time at ETH! 10/10!*

**Luca, author:** *I highly recommend this to anyone who realizes that their German language skills are slipping during their ETH studies.*

**Anna, author:** *I actually applied as an author to the hottest paper in Switzerland: the "Nidwaldner blitz". I typed in the wrong e-mail address - bang. Now I'm trapped in the amiv-blitz cult. I'm still waiting in vain for the free semester meal.*

**Antonia, author:** *In the 4th semester, I spent 10 hours copying out my quantum mechanics summary and got so upset about the unnecessary nature of this activity that I had to write a blitz article about handwritten summaries. Since the structures at ETH are outdated, slow and often nonsensical, there was enough material for more Rant articles.*

**Alexandra, author:** *You can write what you want? You don't have to be able to put commas correctly? The blitz is my place for ranting, recommendations and philosophy💫*

**Bratan, poet:** *I was always frustrated that nobody read my poems. Now at least the person who edits them gets to see them.*

**Michi, author:** *Entertaining editorial meetings, good food & I got to see the inside of a nuclear power plant on a field trip. 11/10, I would recommend to a friend.*

**Leo, Layouter:** *I started at blitz as a lonely mechanical engineer who couldn't do anything. Over time, I got to know more and more people (the blitz team is pretty great, by the way) and my graphics skills reached unprecedented heights. Great bonus: thanks to the dedicated people in the editorial team, you always have your finger on the pulse of what's happening at ETH. If you want to learn something, feel the need to show off your writing skills or just want to meet awesome people, then I can highly recommend the blitz editorial team!*

**Fabienne, former editor-in-chief, layout designer and proofreader:** *During my time at blitz, I not only got to know a lot of great people, but also gained important experience for my future career. During job interviews, I am regularly asked about my involvement with blitz and therefore have the advantage of being able to describe my qualifications using concrete examples from everyday life at the blitz editorial office.*

**Tom, distributor:** *As part of blitz, I was able to get to know many new aspects of ETH, such as underground corridors, study places I didn't know yet and meeting rooms where people eat pizza :) It was also a valuable experience to see how quickly people take you seriously if you appear serious enough while handing out blitz issues.*

**Johan, cover designer:** *Creativity at the touch of a button? I can't. But I have to, because every other week a new issue with an even more stupid title is thrown onto the market and in a weak minute I agreed to do the covers. Apart from that? Cool people, tasty sessions, looking at exam results before anyone else...*

**Charlotte, Lektorin:** *Ich mag es den blitz zu lesen. Leider lese ich sehr schnell und dann ist er ausgelesen :(. Deswegen brauchte ich eine Ausrede, ihn noch früher zu lesen. Jetzt bin ich Lektorin, das macht Spass. Zum Glück machen die Leute vom blitz ganz viele Kommafehler, dann, habe, ich, auch, was, zu, tun :)*

**Alex, <span className="line-through">VCS-Spion</span> Autor:** *Wie funktioniert Pornhub? Was ist die Reaktionsenthalpie von Ammoniumnitrat bei Verbrennung? Was, Linux kann mittlerweilen mehr als ein Tamagotchi? Interessiert. Absolut. Niemanden. Das habe ich im Gespräch mit Freunden gelernt. Aber guess what? Im blitz wird mein Scheiss sogar lektoriert, und hin und wieder erhalte ich Emails mit Feedback.*

**Jose, graduate writer:** *I was originally recruited for the blitz from the common room, back when I didn't know that many people in AMIV. I'm not a particularly great writer, so my interest in joining the blitz was mainly for the free lunch at the editorial meeting every 2 weeks. In the beginning I did puzzles for the blitz, but later I did some "quality journalism". For example, I met some professors for interviews. For example, Professor Angst for the Angstblitz. Later on, I was a quaestor for six months, where I gained new sponsors and lost others. That was a particularly exciting experience, and even though it's not actually a lot of work, I was able to score very well on my CV. During this time, I wrote fewer articles myself, but rather got other people who don't regularly write for blitz but have interesting stories to contribute to blitz. A special plus was that as a representative of blitz I was able to go to the freshman weekend, which I had missed at the beginning of my studies.*

  `;
  return <MDXRemote source={content} components={components} />;
}
