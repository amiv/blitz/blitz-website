import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function MainDE() {
  const content = `
## Erscheinungsdaten und Redaktionsschluss für das Frühjahrssemester 2025

- **blitz 07: Ja/Nein/Vielleicht**

  Red. Schluss 26.02.2025, Erscheinungsdatum 10.03.2025
- **blitz 08: mavt(at)ethz.ch**

  Red. Schluss 12.03.2025, Erscheinungsdatum 24.03.2025
- **blitz 09: Nutri Score**

  Red. Schluss 26.03.2025, Erscheinungsdatum 07.04.2025
- **blitz 10: Hasskommentarschmiede**

  Red. Schluss 09.04.2025, Erscheinungsdatum 28.04.2025
- **blitz 11: Brainrot**

  Red. Schluss 30.04.2025, Erscheinungsdatum 12.05.2025
- **blitz 12: Glücksspiel**

  Red. Schluss 14.05.2025, Erscheinungsdatum 26.05.2025

## Weitere Ausgaben im [Archiv](https://blitz-archive.amiv.ethz.ch/)

Die Geschichte des blitz geht zurück bis in die 60er Jahre. Alle alten Ausgaben, und auch die Neuen, findet man in unserem [Archiv](https://blitz-archive.amiv.ethz.ch/).

Den blitz04 - Hörbuch des HS2021 gibt es auch als HÖRBUCH: [Hörbuch](https://blitz.ethz.ch/grav/hoerbuch/)

## Wir suchen Mitglieder!

Liebe AMIV-Mitglieder

Der blitz sucht schreibende Verstärkung! Möchtest du Dampf ablassen über dein Studium, hast du ein Projekt von dem zu wenig Menschen wissen, bist du von mitteilungsbedürftigem Weltschmerz geplagt, bietet dir das Studium zu wenig Möglichkeiten deiner Kreativität in Form von druckbaren Erzeugnissen freien Lauf zu lassen oder schmeisst du gerne Buchstaben aufs Papier in der Hoffnung, dass etwas lesbares rauskommt? Dann und auch sonst bist Du bei uns genau richtig!

Im blitz erwartet dich eine nette Redaktion, ein pompöses Semesterendessen bei aktiver Mitarbeit und vielleicht mehr. Melde Dich bei Interesse unter [info@blitz.ethz.ch](mailto:info@blitz.ethz.ch)

Dutzende aktive und ehemalige Mitglieder der Redaktion bestätigen die Vorteile des Engagements:

**Alexander, Ex-Chefredaktor:** *Eigentlich wollte ich ja Ausgaben verteilen um gratis chinesisch zu Essen, jetzt darf ich über die grösste Fachvereinszeitschrift an der ETH herrschen und solche Aufrufe schreiben, 10/10*

**Manuel, langjähriges Redaktionsmitglied:** *Welche Fachvereinszeitschrift? Willst du mir sagen, du hast meine Texte veröffentlicht, die ich dir jeweils gesendet habe? Ist das öffentlich? Jetzt wird mir auch klar, weshalb mich nach dem Background-Check keine Firma einstellen will! 0/10*

**Julian, ehemaliger Layouter und Quästor:** *In meiner Zeit im blitz konnte ich nicht nur super Leute kennenlernen, eine tolle Zeit haben und gratis Mittagessen geniessen, sondern auch mich selbst verwirklichen und eine Spur hinterlassen. Egal was du beim blitz machst, es lohnt sich! Und zwar gleich doppelt: einerseits für dich, indem du deine eigenen kreativen Ideen umsetzen kannst, andererseits für deine Mitstudierenden, welche alle zwei Wochen etwas Ablenkung oder interessante Einblicke erleben dürfen. Komm am besten gleich an eine Redaktionssitzung um Deinen Impact während deiner ETH Zeit zu hinterlassen! 10/10!*

**Luca, Autor:** *Kann ich all jenen sehr empfehlen, die im Laufe ihres ETH-Studiums merken, dass sich ihre Deutschkenntnisse verabschieden.*

**Anna, Autorin:** *Als Autorin beworben habe ich mich eigentlich beim heissesten Blatt der Schweiz: dem "Nidwaldner blitz". Falsche Mailadresse eingetippt - zack. Jetzt bin ich in der amiv-blitz-Sekte gefangen. Auf das gratis Semesteressen warte ich noch vergebens.*

**Antonia, Autorin:** *Im 4. Semester habe ich 10h lang meine Quantenmechanik Zusammenfassung abgeschrieben und mich dabei so sehr über die Unnötigkeit dieser Tätigkeit aufgeregt, dass ich einen blitzartikel über handgeschriebene Zusammenfassungen schreiben musste. Da die Strukturen an der ETH veraltet, langsam und oft unsinnig sind, gab es genug Stoff für weitere Rant-Artikel.*

**Alexandra, Autorin:** *Man kann schreiben was man will? Man muss keine Kommas richtig, setzen können? Der blitz ist mein Örtchen für Motzerei, Empfehlungen und Philosophie💫*

**Bratan, Lyriker:** *Dass niemand meine Gedichte las, hat mich immer frustriert. Nun bekommt sie wenigstens der zu Gesicht, der sie lektoriert.*

**Michi, Autor:** *Unterhaltsame Redaktionssitzungen, gutes Essen & ich konnte bei einem Ausflug ein AKW von Innen sehen. 11/10, würde ich einem Bekannten weiterempfehlen.*

**Leo, Layouter:** *Angefangen beim blitz habe ich als einsamer, nichts könnender Maschinenbauer. Im Laufe der Zeit lernte ich mehr und mehr Leute kennen (das blitz-Team ist ziemlich toll, imfall) und auch meine Grafikskills erreichten noch nie dagewesene Höhen. Toller Bonus: dank der engagierten Menschen in der Redaktion ist man immer am Puls des ETH-Geschehens. Wenn du irgendwas lernen möchtest, das Bedürfnis hast, deine Schreibfähigkeiten zur Schau zu stellen oder einfach super Leuten begegnen willst, dann kann ich dir die blitz-Redaktion wärmstens empfehlen!*

**Fabienne, Ehemalige Chefredaktorin, Layouterin und Lektorin:** *Durch mein Engagement beim blitz habe ich nicht nur viele tolle Leute kennen gelernt, sondern auch wichtige Erfahrungen für mein späteres Berufsleben sammeln können. Bei Vorstellungsgesprächen werde ich regelmässig auf mein Engagement beim blitz angesprochen und habe so den Vorteil, meine Qualifikationen anhand von konkreten Beispielen aus dem Alltag der blitz Redaktion beschreiben zu können.*

**Tom, Verteiler:** *Als Teil vom blitz konnte ich viele neue Seiten der ETH kennenlernen, wie unterirdische Gänge, Studienplätze die ich noch nicht kannte und Sitzungszimmer, in denen Pizza gegessen wird :) Ausserdem war es eine wertvolle Erfahrung zu sehen, wie schnell Leute einen Ernst nehmen, wenn man genug seriös wirkt, während man blitz-Ausgaben verteilt.*

**Johan, Coverdesigner:** *Kreativität auf Knopfdruck? Kann ich nicht. Muss ich aber, weil jede zweite Woche eine neue Ausgabe mit noch bescheuerterem Titel auf den Markt geworfen wird und ich in einer schwachen Minute mich dazu bereiterklärt hatte, die Covers zu machen. Ansonsten? Coole Leute, leckere Sitzungen, Prüfungsergebnisse vor allen anderen anschauen…*

**Charlotte, Lektorin:** *Ich mag es den blitz zu lesen. Leider lese ich sehr schnell und dann ist er ausgelesen :(. Deswegen brauchte ich eine Ausrede, ihn noch früher zu lesen. Jetzt bin ich Lektorin, das macht Spass. Zum Glück machen die Leute vom blitz ganz viele Kommafehler, dann, habe, ich, auch, was, zu, tun :)*

**Alex, <span className="line-through">VCS-Spion</span> Autor:** *Wie funktioniert Pornhub? Was ist die Reaktionsenthalpie von Ammoniumnitrat bei Verbrennung? Was, Linux kann mittlerweilen mehr als ein Tamagotchi? Interessiert. Absolut. Niemanden. Das habe ich im Gespräch mit Freunden gelernt. Aber guess what? Im blitz wird mein Scheiss sogar lektoriert, und hin und wieder erhalte ich Emails mit Feedback.*

**Jose, fertigstudierte Autorin:** *Für den blitz bin ich ursprünglich aus dem Aufenthaltsraum rekrutiert worden, damals kannte ich noch nicht so viele Leute im AMIV. Ich bin kein besonders grossartiger Schreiber, daher galt mein Interesse beim blitz mitzumachen vor allem dem gratis Mittagessen bei der Redaktionssitzung alle 2 Wochen. Am Anfang habe ich Rätsel für den blitz gemacht, später dann doch den ein oder anderen “Qualitätsjournalismus”. Dabei habe ich zum Beispiel einige Professoren zu Interviews getroffen. Zum Beispiel Professor Angst für den Angstblitz. Später bin ich dann für ein halbes Jahr Quästor gewesen, wo ich sowohl neue Sponsoren gewonnen, als auch andere verloren hatte. Das war eine besonders spannende Erfahrung, und auch wenn es eigentlich nicht besonders viel Aufwand ist, konnte ich damit auf meinem CV sehr gut punkten. In der Zeit habe ich weniger selbst Artikel geschrieben, sondern habe mehr andere Menschen, die nicht regelmassig für den blitz schreiben, aber interessante Stories haben, dazu gebracht um mal einen Beitrag für den blitz zu machen. Ein besonderes Plus war, dass ich als Vertreter vom blitz auch nochmal mit aufs Erstiweekend konnte, was ich am Anfang vom Studium verpasst hatte.

`;
  return <MDXRemote source={content} components={components} />;
}
