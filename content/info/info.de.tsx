import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function InfoDE() {
  const content = `
Der blitz ist die Fachzeitschrift des [AMIV an der ETH](https://amiv.ethz.ch). Der AMIV vertritt die Belange der Maschineningenieurwissenschaften (D-MAVT) und Elektrotechnik und Informationstechnologie (D-ITET) Studierenden der ETH Zürich vertritt. Die Auflage in einem regulären Semester beträgt 1100. Während dem HS22 existierte der blitz nur online, da aufgrund der Bestimmungen des Bundes und der ETH bezüglich der Lehre, eine gedruckte Ausgabe mit dem bisherigen Verfahren des Verteilens der Zeitschriften nicht möglich war.

## In einem regulären Semester sind die Ausgaben an folgenden Orten an der ETH zu finden:

![image](/images/Karte_V2.svg)

Die Plätze sind dabei die folgenden:

- CAB – Aufenthaltsraum (E 32) direkt beim Eingang
- CHN – Bei der Infowand vor dem Zwei Grad Bistro
- ML – F-Stock gegenüber den F-Hörsälen, bei der Infowand
- ML – E-Stock beim grossen Hörsaal (D 28), neben der Tannenbar
- CLA – Im Eingang bei der Drehtüre
- ETF – Vor dem Eingang des E1 Hörsaals
- ETF – Vor dem Eingang des C1 Hörsaals
- ETZ – E-Stock neben Kiosk bei der Infowand (vor ETA Eingang)

  `;
  return <MDXRemote source={content} components={components} />;
}
