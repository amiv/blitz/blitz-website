import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function InfoEN() {
  const content = `
Der blitz is the journal of the [AMIV at ETH](https://amiv.ethz.ch). The AMIV represents the interests of mechanical engineering (D-MAVT) and electrical engineering and information technology (D-ITET) students at ETH Zurich. The circulation in a regular semester is 1100. During HS22 the blitz only existed online, as due to federal and ETH regulations regarding teaching, a printed edition was not possible with the previous method of distributing the journals.

## In a regular semester, the issues can be found at the following locations at ETH:

![image](/images/Karte_V2.svg)

The places are as follows:

- CAB - Break Room (E 32) directly at the entrance
- CHN - At the information wall in front of the Zwei Grad Bistro
- ML - F floor opposite the F lecture halls, by the information wall
- ML - E floor by the large lecture hall (D 28), next to the Tannenbar
- CLA - In the entrance by the revolving door
- ETF - In front of the entrance to the E1 lecture hall
- ETF - In front of the entrance to the C1 lecture hall
- ETZ - E-Stock next to the kiosk by the information wall (in front of the ETA entrance)
  `;
  return <MDXRemote source={content} components={components} />;
}
