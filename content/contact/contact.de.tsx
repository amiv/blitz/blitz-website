import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function ContactDE() {
  const content = `
## Redaktion

AMIV an der ETH
blitz-Redaktion
CAB E37
Universitätsstrasse 6
8092 Zürich

## Kontakt

Der Fernschreiber ist ausser Betrieb, die Brieftauben kommen bei gutem Wetter und offenem Fenster an, das Morsegerät ist zur Zeit in der Wartung und telefonisch sind wir sehr schwer zu erreichen. E-Mail ist das Medium der Wahl!

**Allgemein:** [info@blitz.ethz.ch](mailto:info@blitz.ethz.ch)

**Redaktion:** [artikel@blitz.ethz.ch](mailto:artikel@blitz.ethz.ch)

**Fragen zu Werbung und Inseraten:** [werbung@blitz.ethz.ch](mailto:werbung@blitz.ethz.ch)

**Redaktionsmitglieder:**

- Redaktionsleitung: Andreas [praesident@blitz.ethz.ch](mailto:praesident@blitz.ethz.ch)
- Werbung & Finanzen: Nicolai [werbung@blitz.ethz.ch](mailto:werbung@blitz.ethz.ch)
- Layout: Julia, Julie & Nic [layout@blitz.ethz.ch](mailto:layout@blitz.ethz.ch)

**Druckerei** Normale Ausgaben Schellenberg Druck AG, Pfäffikon
  `;
  return <MDXRemote source={content} components={components} />;
}
