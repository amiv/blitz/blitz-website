import { MDXRemote } from "next-mdx-remote/rsc";

import { components } from "@/utils/markdownComponents";

export default function ContactEN() {
  const content = `
## Editorial Office

AMIV an der ETH
blitz-Redaktion
CAB E37
Universitätsstrasse 6
8092 Zürich

## Contact us

The teleprinter is out of order, the carrier pigeons arrive when the weather is good and the window is open, the Morse code machine is currently undergoing maintenance and we are very difficult to reach by telephone. E-mail is the medium of choice!

**General:** [info@blitz.ethz.ch](mailto:info@blitz.ethz.ch)

**Editorial Office:** [artikel@blitz.ethz.ch](mailto:artikel@blitz.ethz.ch)

**Questions about Adverts:** [werbung@blitz.ethz.ch](mailto:werbung@blitz.ethz.ch)

**Editorial Staff:**

- Editorial Management: Andreas [praesident@blitz.ethz.ch](mailto:praesident@blitz.ethz.ch)
- Advertising & Finance: Nicolai [werbung@blitz.ethz.ch](mailto:werbung@blitz.ethz.ch)
- Layout: Julia, Julie & Nic [layout@blitz.ethz.ch](mailto:layout@blitz.ethz.ch)

**Print Shop** Normal Issues Schellenberg Druck AG, Pfäffikon
  `;
  return <MDXRemote source={content} components={components} />;
}
