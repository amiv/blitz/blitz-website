export default function title({ title }: { title: string }) {
  return (
    <div className="w-full text-center px-4 h-96 flex flex-col justify-center shadow-sm dark:border-b dark:border-stone-700 relative">
      <div
        className="bg-grid-slate-900 absolute inset-0 z-0 h-96 dark:hidden block"
        style={{
          backgroundSize: "20px 20px",
          backgroundImage:
            "radial-gradient(circle, #cccccc 1px, rgba(0, 0, 0, 0) 1px)",
          maskImage: "linear-gradient(to bottom, transparent, black)",
          WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
        }}
      />
      <div
        className="bg-grid-slate-900 absolute inset-0 z-0 h-96 hidden dark:block"
        style={{
          backgroundSize: "20px 20px",
          backgroundImage:
            "radial-gradient(circle, #444444 1px, rgba(0, 0, 0, 0) 1px)",
          maskImage: "linear-gradient(to bottom, transparent, black)",
          WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
        }}
      />
      <div />
      <div className="z-10">
        <h1 className="font-black black text-5xl sm:text-7xl">{title}</h1>
      </div>
    </div>
  );
}
