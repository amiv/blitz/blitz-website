import Link from "next/link";

import { headers } from "next/headers";

import { getI18n } from "@/locales/server";

import LanguageMenu from "./languageMenu";
import NavLinksDesktop from "./navLinksDesktop";
import MobileNav from "./mobileNav";

export default async function Navbar({ locale }: { locale: string }) {
  const t = await getI18n();

  const navItems = [
    {
      label: t("home"),
      path: "/",
    },
    {
      label: t("join"),
      path: "/join",
    },
    {
      label: t("info"),
      path: "/info",
    },
    {
      label: t("advertising"),
      path: "/advertising",
    },
    {
      label: t("archive"),
      path: "/archive",
    },
    {
      label: t("contact"),
      path: "/contact",
    },
  ];

  return (
    <nav className="w-full px-6 pt-6 shadow-md bg-white dark:bg-stone-800 text-black dark:text-stone-200 dark:border-b dark:border-stone-600">
      <div className="w-full sm:h-12 h-8 flex justify-between">
        <Link href="/">
          <img
            className="block dark:hidden h-full"
            src="/logo.svg"
            alt="blitz Logo"
          />
          <img
            className="hidden dark:block h-full"
            src="/logo-white.svg"
            alt="blitz Logo"
          />
        </Link>
        <div className="h-full relative flex flex-col items-end">
          <LanguageMenu locale={locale} />
        </div>
      </div>
      <NavLinksDesktop navItems={navItems} />
      <MobileNav navItems={navItems} />
    </nav>
  );
}
