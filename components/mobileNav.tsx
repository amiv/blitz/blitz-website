"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

import { useState } from "react";

import { IconMenu2, IconX } from "@tabler/icons-react";

import { useI18n } from "@/locales/client";

export default function MobileNav({
  navItems,
}: {
  navItems: { label: string; path: string }[];
}) {
  const pathnameFull = usePathname();
  const pathname = pathnameFull.slice(4);

  const [mobileNavOpen, setMobileNavOpen] = useState<boolean>(false);

  return (
    <div className="block sm:hidden w-full h-fit">
      <div className="h-16 flex flex-col justify-center">
        <button
          className="flex"
          onClick={() => setMobileNavOpen(!mobileNavOpen)}
        >
          {mobileNavOpen ? <IconX /> : <IconMenu2 />}
          <span className="ml-2">Menu</span>
        </button>
      </div>

      <div
        className={`overflow-y-hidden ${!mobileNavOpen ? "hidden" : "block"}`}
      >
        <div className="w-full pb-6">
          <div className="bg-stone-100 dark:bg-stone-700 rounded-md px-6 py-6">
            <ul>
              {navItems.map((item) => (
                <li
                  key={item.path}
                  className={`block px-3 py-2 text-xl ${item.path.slice(1) === pathname && "bg-black dark:bg-stone-100 dark:text-black text-white rounded-md font-semibold dark:border-stone-200 shadow-md"}`}
                >
                  <Link href={item.path}>{item.label}</Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
