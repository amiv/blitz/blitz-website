"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

import { useI18n } from "@/locales/client";

export default function NavLinksDesktop({
  navItems,
}: {
  navItems: { label: string; path: string }[];
}) {
  const pathnameFull = usePathname();
  const pathname = pathnameFull.slice(4);

  return (
    <div className="hidden sm:flex w-full h-16 flex-col justify-end">
      <ul className="flex">
        {navItems.map((item) => (
          <li
            key={item.path}
            className={`block mr-8 pb-2 ${item.path.slice(1) === pathname && "border-b-2 border-black font-semibold dark:border-stone-200"}`}
          >
            <Link href={item.path}>{item.label}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
