"use client";

import Link from "next/link";

import { Menu } from "@headlessui/react";

import { useChangeLocale } from "@/locales/client";

import { IconChevronDown, IconChevronUp } from "@tabler/icons-react";

export default function LanguageMenu({ locale }: { locale: string }) {
  const locales: ("de" | "en")[] = ["de", "en"];
  const changeLocale = useChangeLocale();

  return (
    <Menu>
      {({ open }) => (
        <>
          <Menu.Button className="flex sm:mt-3 mt-1">
            <span>{locale.toUpperCase()}</span>
            {open ? (
              <IconChevronUp className="ml-2" />
            ) : (
              <IconChevronDown className="ml-2" />
            )}
          </Menu.Button>
          <Menu.Items className="bg-black w-48 bg-white dark:bg-stone-700 p-2 rounded-md text-lg shadow-md">
            {locales.map((l, i) => (
              <Menu.Item key={i}>
                {({ active }) => (
                  <button
                    className={`${l === locale && "bg-black dark:bg-stone-100 text-white dark:text-black font-semibold"} ${active && l !== locale && "bg-stone-100 dark:bg-stone-600"} w-full text-left p-2 rounded-md`}
                    onClick={() => changeLocale(l)}
                  >
                    {l.toUpperCase()}
                  </button>
                )}
              </Menu.Item>
            ))}
          </Menu.Items>
        </>
      )}
    </Menu>
  );
}
