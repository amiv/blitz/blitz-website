import type { Metadata, ResolvingMetadata } from "next";

import Title from "@/components/title";

import { getI18n, getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import ArchiveDE from "@/content/archive/archive.de";
import ArchiveEN from "@/content/archive/archive.en";

export function generateStaticParams() {
  return getStaticParams();
}

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Archiv | blitz – Fachzeitschrift des AMIV an der ETH",
        description:
          "Webseite des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title:
          "Archive | blitz - The student association magazine of AMIV at ETH",
        description:
          "Website of blitz, the student association magazine of AMIV at ETH",
      };
}

export default async function Advertising({
  params: { locale },
}: {
  params: { locale: "de" | "en" };
}) {
  setStaticParamsLocale(locale || "en");
  const t = await getI18n();

  return (
    <div className="w-full">
      <Title title={t("archive")} />

      <div className="w-full max-w-[1200px] mx-auto p-6">
        {locale === "en" ? <ArchiveEN /> : <ArchiveDE />}
      </div>
    </div>
  );
}
