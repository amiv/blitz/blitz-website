import type { Metadata, ResolvingMetadata } from "next";

import Title from "@/components/title";

import { getI18n, getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import ContactDE from "@/content/contact/contact.de";
import ContactEN from "@/content/contact/contact.en";

export function generateStaticParams() {
  return getStaticParams();
}

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Kontakt | blitz – Fachzeitschrift des AMIV an der ETH",
        description:
          "Webseite des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title:
          "Contact Us | blitz - The student association magazine of AMIV at ETH",
        description:
          "Website of blitz, the student association magazine of AMIV at ETH",
      };
}

export default async function Contact({
  params: { locale },
}: {
  params: { locale: "de" | "en" };
}) {
  setStaticParamsLocale(locale || "en");
  const t = await getI18n();

  return (
    <div className="w-full">
      <Title title={t("contact")} />

      <div className="w-full max-w-[1200px] mx-auto p-6">
        {locale === "en" ? <ContactEN /> : <ContactDE />}
      </div>
    </div>
  );
}
