import Image from "next/image";
import Link from "next/link";

import { MDXRemote } from "next-mdx-remote/rsc";

import { getI18n, getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import MainDE from "@/content/main/main.de";
import MainEN from "@/content/main/main.en";

import { IconChevronDown, IconDownload } from "@tabler/icons-react";

export function generateStaticParams() {
  return getStaticParams();
}

async function getBlitzData() {
  // revalidate data after 1h
  const res = await fetch(
    "https://blitz-archive-backend.amiv.ethz.ch/overview",
    { next: { revalidate: 3600 } },
  );

  if (!res.ok) {
    throw new Error("Fetching Data from blitz archive backend failed.");
  }

  return res.json();
}

export default async function Home({
  params: { locale },
}: {
  params: { locale: "de" | "en" };
}) {
  setStaticParamsLocale(locale || "en");

  const t = await getI18n();
  const issues = await getBlitzData();

  const mostRecentIssues = (() => {
    if (!issues) return [];
    const keys = Object.keys(issues.overview);
    const allIssues = Object.keys(issues.overview)
      .flatMap((a) =>
        Object.keys(issues.overview[a]).flatMap((b) =>
          Object.keys(issues.overview[a][b]),
        ),
      )
      .toSorted((a, b) => (a > b ? -1 : 1));
    return allIssues.slice(0, 7);
  })();

  const currentIssue = mostRecentIssues[0];

  const getPreview = (name: string) => {
    // because this one has the best cover
    if (!name)
      return "https://blitz-archive-backend.amiv.ethz.ch/preview/blitz-2017-HS-04-blitz_for_her";
    return `https://blitz-archive-backend.amiv.ethz.ch/preview/${name}`;
  };

  const getUrl = (name: string) => {
    if (!name) return undefined;
    return `https://blitz-archive-backend.amiv.ethz.ch/blitz/${name}`;
  };

  const getDescription = (name: string) => {
    const split = name.split("-");
    return `${t("issue")} ${split[3]}, ${split[2]}${split[1].slice(2)}`;
  };

  return (
    <div className="w-full relative">
      <div className="w-full text-center px-4 h-[calc(100vh-136px)] flex flex-col justify-between shadow-md dark:border-b dark:border-stone-700">
        <div
          className="bg-grid-slate-900 absolute inset-0 z-0 h-[calc(100vh-136px)] block dark:hidden"
          style={{
            backgroundSize: "20px 20px",
            backgroundImage:
              "radial-gradient(circle, #cccccc 1px, rgba(0, 0, 0, 0) 1px)",
            maskImage: "linear-gradient(to bottom, transparent, black)",
            WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
          }}
        />
        <div
          className="bg-grid-slate-900 absolute inset-0 z-0 h-[calc(100vh-136px)] hidden dark:block"
          style={{
            backgroundSize: "20px 20px",
            backgroundImage:
              "radial-gradient(circle, #444444 1px, rgba(0, 0, 0, 0) 1px)",
            maskImage: "linear-gradient(to bottom, transparent, black)",
            WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
          }}
        />
        <div />
        <div className="z-10 text-center">
          <Image
            unoptimized
            src="/signet.svg"
            className="mx-auto block dark:hidden sm:w-32 w-24"
            alt="blitz Signet"
            width={128}
            height={128}
          />
          <Image
            unoptimized
            src="/signet-white.svg"
            className="mx-auto hidden dark:block sm:w-32 w-24"
            alt="blitz Signet light"
            width={128}
            height={128}
          />
          <h1 className="font-black text-4xl sm:text-7xl">
            {t("welcomeToBlitz")}
          </h1>
          <p className="font-semibold text-xl sm:text-2xl">
            {t("studentMagazine")}
          </p>
        </div>
        <div className="z-10 text-center">
          <p className="font-semibold text-lg">est. 1966</p>
        </div>
        <div className="flex justify-center z-10">
          <Link
            href="#content"
            className="block w-fit"
            aria-label="Link to main Content"
          >
            <IconChevronDown size={96} className="animate-bounce" />
          </Link>
        </div>
        <div />
      </div>

      <div
        id="content"
        className="w-full mt-8 shadow-md dark:border-b dark:border-stone-700"
      >
        <div className="max-w-[1200px] mx-auto px-6 text-center sm:text-left">
          <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 flex items-center">
            <div className="flex justify-center">
              <div className="w-96 max-w-full rounded-md overflow-hidden shadow-2xl border border-neutral-200">
                <a href={getUrl(currentIssue)} target="_blank">
                  <Image
                    src={getPreview(currentIssue)}
                    alt="Cover of newest blitz"
                    className="w-full"
                    width={384}
                    height={543}
                  />
                </a>
              </div>
            </div>
            <div>
              <h2 className="font-black sm:text-5xl text-4xl">
                {t("currentIssue")}
              </h2>
              <p className="text-lg">{getDescription(currentIssue)}</p>
              <a
                className="mt-2 inline-flex items-center rounded-md hover:bg-neutral-800 bg-black px-3 py-2 text-sm font-semibold text-white dark:text-black dark:bg-neutral-200 dark:hover:bg-neutral-300 shadow"
                href={getUrl(currentIssue)}
                target="_blank"
              >
                <IconDownload size={16} />
                <span className="ml-2">{t("view")}</span>
              </a>
            </div>
          </div>
        </div>

        <div className="w-full pt-16 pb-8 shadow-md px-6">
          <div className="max-w-[1200px] mx-auto">
            <h2 className="font-black sm:text-5xl text-4xl">
              {t("newestIssues")}
            </h2>

            <div className="grid grid-cols-1 sm:grid-cols-3 gap-4 my-8">
              {mostRecentIssues.slice(1).map((i) => (
                <div
                  key={i}
                  className="w-96 max-w-full rounded-md overflow-hidden shadow-2xl border border-neutral-200 mx-auto"
                >
                  <a href={getUrl(i)} target="_blank">
                    <Image
                      className="w-full"
                      src={getPreview(i)}
                      alt={`Cover of ${getDescription(i)}`}
                      width={384}
                      height={543}
                    />
                  </a>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>

      <div className="max-w-[1200px] w-full mx-auto pt-8 px-6 pb-6">
        {locale === "en" ? <MainEN /> : <MainDE />}
      </div>
    </div>
  );
}
