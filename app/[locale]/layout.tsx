import type { Metadata, ResolvingMetadata } from "next";
import { Inter } from "next/font/google";

import Navbar from "@/components/navbar";
import Footer from "@/components/footer";

const inter = Inter({ subsets: ["latin"] });

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "blitz – Fachzeitschrift des AMIV an der ETH",
        description:
          "Webseite des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title: "blitz - The student association magazine of AMIV at ETH",
        description:
          "Website of blitz, the student association magazine of AMIV at ETH",
      };
}

export default function RootLayout({
  children,
  params: { locale },
}: Readonly<{
  children: React.ReactNode;
  params: { locale: string };
}>) {
  return (
    <html lang="en">
      <body className={`${inter.className} bg-white dark:bg-stone-800 min-h-screen flex flex-col justify-between`}>
        <Navbar locale={locale} />
        <main className="flex grow mt-4 bg-white dark:bg-stone-800 flex-col items-center justify-between text-black dark:text-stone-200">
          {children}
        </main>
        <Footer locale={locale as "de" | "en"} />
      </body>
    </html>
  );
}
