import type { Metadata, ResolvingMetadata } from "next";

import Title from "@/components/title";

import { getI18n, getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import JoinDE from "@/content/join/join.de";
import JoinEN from "@/content/join/join.en";

export function generateStaticParams() {
  return getStaticParams();
}

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  const locale = params.locale;

  return locale === "de"
    ? {
        title: "Mitmachen | blitz – Fachzeitschrift des AMIV an der ETH",
        description:
          "Webseite des blitz, der Fachzeitschrift des AMIV an der ETH",
      }
    : {
        title:
          "Join Us | blitz - The student association magazine of AMIV at ETH",
        description:
          "Website of blitz, the student association magazine of AMIV at ETH",
      };
}

export default async function Join({
  params: { locale },
}: {
  params: { locale: "de" | "en" };
}) {
  setStaticParamsLocale(locale || "en");
  const t = await getI18n();

  return (
    <div className="w-full">
      <Title title={t("join")} />

      <div className="w-full max-w-[1200px] mx-auto p-6">
        {locale === "en" ? <JoinEN /> : <JoinDE />}
      </div>
    </div>
  );
}
