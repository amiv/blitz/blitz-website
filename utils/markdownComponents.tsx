export const components = {
  h2: (props: any) => (
    <h2 className="font-black text-3xl sm:text-4xl mt-8 mb-4">
      {props.children}
    </h2>
  ),
  p: (props: any) => <p className="my-2">{props.children}</p>,
  a: (props: any) => (
    <a className="underline" href={props.href}>
      {props.children}
    </a>
  ),
  strong: (props: any) => (
    <strong className="font-semibold">{props.children}</strong>
  ),
  ul: (props: any) => <ul className="list-disc ml-8">{props.children}</ul>,
  img: (props: any) => (
    <img
      className="w-[560px] max-w-full rounded-md shadow-md mx-auto border border-neutral-300"
      src={props.src}
    />
  ),
};
